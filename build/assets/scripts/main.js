"use strict";

var active = "calc__option--is-active",
    active2 = "calc__option--is-active2",
    price = document.getElementById("price").children;

function changePrice(cost, $this) {
  var result = cost.split(".");
  price[0].innerHTML = result[0];
  price[1].innerHTML = "'" + result[1];
  var current = document.querySelector("." + active);
  current ? current.classList.remove(active) : null;
  $this.classList.add(active);
}

function changeSymbol(symbol, $this) {
  var current = document.querySelector("." + active2);
  symbol ? price[2].innerHTML = "*" : price[2].innerHTML = "";
  current ? current.classList.remove(active2) : null;
  $this.classList.add(active2);
}

$("#config").click(function () {
  $([document.documentElement, document.body]).animate({
    scrollTop: $("#calc").offset().top
  }, 1000);
});